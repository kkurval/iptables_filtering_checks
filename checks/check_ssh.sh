#!/bin/bash

# Check if SSH ports are opened

RESULTS="/root/results"
LAST="false"

while true; do

	ssh root@192.168.6.2 "iptables -n -L|grep :22|grep ACCEPT" &> /dev/null

	RESULT=$?

	if [ $RESULT -ne 0 ] && [ $LAST == "false" ]; then
		echo "Failed to find ssh info"
	elif [ $RESULT -eq 0 ] && [ $LAST == "false" ]; then
		echo "Found ssh info"
		touch $RESULTS/ssh_ports
		LAST="true"
	elif [ $RESULT -ne 0 ] && [ $LAST == "true" ]; then
		echo "SSH has been removed"
		rm $RESULTS/ssh_ports
		LAST="false"
       	elif [ $RESULT -eq 0 ] && [ $LAST == "true" ]; then
		echo "SSH has been set"
	else
		echo "Something went wrong!"
	fi

	sleep 5

done
