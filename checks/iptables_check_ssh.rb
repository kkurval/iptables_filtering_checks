#!/usr/bin/env ruby

respath = '/root/results/iptables_checks_ssh.txt'

ret = `hping3 -a 192.168.6.254 -p 22 --syn -c 1 192.168.6.1`
if ret.to_s.include?('flags=SA')
  File.open(respath, 'w') {|f| f.write('IPtables: SSH port accessible!') }
else
  if File.exist? respath
    File.delete respath
  end
end

